@extends('layouts.master')

@section('content')
<!-- Content Header (Page header) -->
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    {{ Session::get('success') }}
  </div>
@elseif(Session::has('error'))
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    {{ Session::get('error') }}
  </div>
@endif
<section class="content-header">
  <h1>
    Location
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Maps</h3>
          <div class="clearfix"></div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="btn-group">
                <button class="btn btn-primary mr-2" id="btn-get-location"> Get My Location </button>
              </div>
            </div>
          </div>

          <div class="row text-center">
            <div class="col-md-12">
              <div id="map-canvas" style="height: 300px; border: 1px solid #000;">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-9">
              <Label for="lat" class="md-3">Latitude</Label>
              <input type="text" name="lat" id="lat" class=" form-control" readonly>
            </div>
            <div class="col-md-9">
              <Label for="long" class="md-3">Longitude</Label>
              <input type="text" name="long" id="long" class="form-control col-md-6" readonly>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

@endsection

@section('script')
<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyBJ0we8EA8ad4E5oxEHwH8fbVrC97a4z2k$callback=InitMap"></script> -->
<script src="//maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;key=AIzaSyDxj_jy97x_3zUDUiZwxSIj2j8bPfACjl8&amp;callback=InitMap" defer></script>
<script src="{{asset('js/location.js')}}"></script>

<script>
  $(document).ready(function(){
    $('#btn-get-location').on('click', function(){
      console.log('lele');
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else { 
        $('#lat').val() = '';
        $('#long').val() = '';
      }
    });

    function showPosition(position) {
      $('#lat').val() = position.coords.latitude;
      $('#long').val() = position.coords.longitude;
    }
  });
</script>

@endsection