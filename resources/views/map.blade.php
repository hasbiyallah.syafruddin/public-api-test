<!DOCTYPE html>
<html>
  <head>
    <title>Places Search Box</title>
    

    <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css' rel='stylesheet' />
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
    <link
        rel="stylesheet"
        href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css"
        type="text/css"
    />
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
  </head>
  <div id='map' style='width: 100vh; height: 600px;'></div>
    <br>
    <div>
        Lat
        <input type="text" disabled id="lat">

    </div>
    <br>
    <div>
        Long
        <input type="text" disabled id="long">

    </div>
    <script>
	mapboxgl.accessToken = 'pk.eyJ1IjoieXVuaWFyZHMiLCJhIjoiY2tjcmpmMHBxMDZidjJ6bzRkajl5Mzg4aSJ9.Spqh3c8-cTVHpCeHfz-AMw';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [106.865036, -6.175110],
        zoom: 11
    });

    var geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl
    })
    
    map.addControl(
        geocoder, 'top-left'
    );

    map.on('load', function() {
        // Listen for the `geocoder.input` event that is triggered when a user
        // makes a selection
        geocoder.on('result', function(ev) {
          var geoCodeRes = ev.result.center

          document.getElementById("lat").value =  geoCodeRes[1]
        document.getElementById("long").value = geoCodeRes[0]
        });

        
    });
</script>
</html>