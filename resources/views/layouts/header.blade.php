<header class="main-header">
  <!-- Logo -->
  <a href="{{route('employee')}}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><img src="{{asset('/images/icon.png')}}" alt=""></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Loyalto-Test</b></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- <a href="" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a> -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user">
          <a class="" href="{{url('/logout')}}"> Logout</a>
        </li>
      </ul>
    </div>
  </nav>
</header>
