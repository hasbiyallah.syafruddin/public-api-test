<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $url ='http://dummy.restapiexample.com/api/v1/employees';
    $data = $this->getCurl($url);

    if (!is_null($data) || !is_null($data['data'])) {
      $dataResult = $this->getResult($data);
    }

    return view('home', compact('dataResult'));
  }

  public function create()
  {
    return view('employee');
  }

  public function post(Request $request)
  {
    $url = "http://dummy.restapiexample.com/api/v1/create";
    $method = "POST";

    return $this->postCurl($request, $url, $method);
  }

  public function detail($id)
  {
    $url ='http://dummy.restapiexample.com/api/v1/employees';
    $data = $this->getCurl($url);

    if (!is_null($data) || !is_null($data['data'])) {
      $dataResult = $this->getResult($data);
    }
    return view('detail', compact('dataResult'));
  }

  public function update($id, Request $request)
  {
    $url = "http://dummy.restapiexample.com/api/v1/update/".$id;
    $method = "PUT";
    return $this->postCurl($request, $url, $method);
  }

  public function getResult($param_data)
  {
    $dataResult = [];
    foreach ($param_data['data'] as $data) {
      $obj = array('id' => $data['id'],
        'employee_name' => $data['employee_name'],
        'employee_salary' => $data['employee_salary'],
        'employee_age' => $data['employee_age']
      );
      $dataResult[] = $obj;
    }

    return $dataResult;
  }

  public function getCurl($url)
  {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if($err){
      return null;
    }else {
      return json_decode($result, true);
    }
  }


  public function postCurl($request, $url, $method)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_POSTFIELDS => "{\t\"name\": \".$request->name.\",\"salary\": \".$request->salary.\",\"age\": \".$request->age.\"}",
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    $msg="";

    if ($method == 'POST') {
      $msg = "Success add new Employee";
    }else if ($method == 'PUT') {
      $msg = "Success update data of Employee";
    }

    if ($err) {
      return redirect()->back()->with('error',$err);
    }else {
      return redirect('/employee')->with('success', $msg);
    }
  }

  public function Location()
  {
     return view('location');
  }
}
