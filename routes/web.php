<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/logout','Controller@logout');

Auth::routes();

Route::get('/employee', 'HomeController@index')->name('employee');
Route::get('/employee/create', 'HomeController@create')->name('emp.create');
Route::get('/employee/detail/{id}', 'HomeController@detail')->name('employee.detail.{id}');
Route::post('/employee/update/{id}', 'HomeController@update')->name('emp.update.{id}');
Route::post('/employee/post', 'HomeController@post')->name('emp.post');

Route::get('/location', 'HomeController@Location');