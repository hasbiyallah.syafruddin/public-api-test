function InitMap(){
    var map = new google.maps.Map($("#map-canvas"), {
        center: new google.maps.LatLng(33.808678, -117.918921),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    // initialize marker
    var marker = new google.maps.Marker({
        position: map.getCenter(),
        draggable: true,
        map: map
    });
    // intercept map and marker movements
    google.maps.event.addListener(map, "idle", function() {
        marker.setPosition(map.getCenter());
        $("#lat").val() = map.getCenter().lat().toFixed(6);
        $("#long").val() = map.getCenter().lng().toFixed(6);
    });

    google.maps.event.addListener(marker, "dragend", function(mapEvent) {
        map.panTo(mapEvent.latLng);
    });
    // initialize geocoder
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener($("#btn-locate"), "click", function() {
        geocoder.geocode({ address: $("#search-input").val() }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var result = results[0];
                $("#search-input").val() = result.formatted_address;
                if (result.geometry.viewport) {
                    map.fitBounds(result.geometry.viewport);
                } else {
                    map.setCenter(result.geometry.location);
                }
            } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                alert("Sorry, geocoder API failed to locate the address.");
            } else {
                alert("Sorry, geocoder API failed with an error.");
            }
        });
    });
    google.maps.event.addDomListener($("#search-input"), "keydown", function(domEvent) {
        if (domEvent.which === 13 || domEvent.keyCode === 13) {
            google.maps.event.trigger($("#btn-locate"), "click");
        }
    });
    // initialize geolocation
    if (navigator.geolocation) {
        google.maps.event.addDomListener($("#btn-detect"), "click", function() {
            navigator.geolocation.getCurrentPosition(function(position) {
                map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            }, function() {
                alert("Sorry, geolocation API failed to detect your location.");
            });
        });
        $("#btn-detect").disabled = false;
    }
}